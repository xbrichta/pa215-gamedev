extends Weapon

var fire_timer: float = 0.0
@export var force: float = 10.0
@export var projectile: Resource
@export var bullet_range: float = 1000.0

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	fire_timer += delta

func attack():
	if fire_timer >= attack_rate:
		var new_projectile: Projectile = projectile.instantiate()
		new_projectile.damage = damage
		new_projectile.position = global_position
		new_projectile.rotation = global_rotation
		new_projectile.bullet_range = bullet_range
		var angle: float = new_projectile.rotation.y
		var direction: Vector3 = Vector3.FORWARD
		direction = direction.rotated(Vector3.UP, angle)
		new_projectile.apply_impulse(direction.normalized() * force * 2)
		get_tree().root.add_child(new_projectile)
		fire_timer = 0.0

extends Weapon

var attack_timer: float = 0.0

var anim: AnimationPlayer

# Called when the node enters the scene tree for the first time.
func _ready():
	anim = $"rotationPoint/AnimationPlayer"
	var sword_dmg_area: DamageArea = $rotationPoint/body/Attack
	sword_dmg_area.damage = damage 
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	attack_timer += delta


func attack():
	if attack_timer >= attack_rate:
		anim.play("Attack")


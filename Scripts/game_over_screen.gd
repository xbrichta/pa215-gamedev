extends CanvasLayer

func _on_button_restart_pressed():
	get_tree().reload_current_scene();


func _on_button_go_to_menu_pressed():
	get_tree().change_scene_to_file("res://Scenes/main_menu.tscn");

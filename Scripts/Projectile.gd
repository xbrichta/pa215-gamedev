extends RigidBody3D
class_name Projectile

var damage: float = 1
var bullet_range: float = 50.0

var start_position: Vector3

func _ready():
	start_position = position
	var dmg_area: DamageArea = $Area3D
	dmg_area.damage = damage

func _process(_delta):
	if position.distance_to(start_position) >= bullet_range:
		queue_free()


func _on_area_3d_body_entered(_body):
	queue_free()


func _on_area_3d_delete():
	queue_free()

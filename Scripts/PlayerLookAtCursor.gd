extends MeshInstance3D

var cursor_screen_position: Vector2

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if get_parent().is_dead:
		return

	var cursor_world_position = get_viewport().get_camera_3d().project_position(cursor_screen_position, 0)
	cursor_world_position.y = global_position.y
	look_at(cursor_world_position)


func _input(event):
	if event is InputEventMouseMotion:
		cursor_screen_position = event.position

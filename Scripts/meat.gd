extends Item

@export var heal_hp: float = 1.0

var timer: float = 0.0
@export var despawn_time: float = 5.0

func _ready():
	timer = 0.0


func _process(delta):
	timer += delta
	if (timer >= despawn_time):
		queue_free()


func on_pick_up(node: Node):
	var health = node.find_child("Health")
	if health != null and health is Health and not health.has_max_hp():
		health.add_health(heal_hp)
		super.on_pick_up(node)
	

extends Node3D
class_name Weapon

@export var attack_rate: float = 0.3
@export var damage: float = 1

func _ready():
	for child in get_children():
		if child is DamageArea:
			child.damage = damage

func attack():
	pass

func give_damage(_health: Health):
	pass

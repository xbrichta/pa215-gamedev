extends Camera3D

@export var player: Node3D
@export var follow_speed: float = 1.0
var cursor_screen_position: Vector2 = Vector2(0, 0)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if player is Node3D:
		var player_position: Vector3 = player.transform.origin
		var cursor_world_position: Vector3 = project_position(cursor_screen_position, 10)
		var target_position: Vector3 = 0.85 * player_position + 0.15 * cursor_world_position
		target_position.y = global_position.y
		global_position = target_position		


func _input(event):
	if event is InputEventMouseMotion:
		cursor_screen_position = event.position

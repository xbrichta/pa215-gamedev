extends Area3D
class_name Stamina

var max_stamina: float = 100
var current_stamina: float
var recharge_speed: float = 100

# Called when the node enters the scene tree for the first time.
func _ready():
	$SubViewport.set_update_mode(SubViewport.UPDATE_WHEN_PARENT_VISIBLE)
	current_stamina = max_stamina
	update_staminabar()


func _process(_delta):
	change_stamina(_delta * recharge_speed)


func can_use_stamina(stamina_cost: float):
	return stamina_cost <= current_stamina;


func update_staminabar():
	var ratio = 100*current_stamina/max_stamina;
	$SubViewport/StaminaBar/ProgressBar.value = ratio


func change_stamina(by_value: float):
	current_stamina = clamp(current_stamina + by_value, 0, max_stamina)
	update_staminabar()
